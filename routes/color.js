const colorController = require('../controllers/colorController')
const router = require('express').Router()

router.get('/:hex', colorController)

module.exports = router